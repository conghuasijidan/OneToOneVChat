//
//  ChatViewController.h
//  OneToOneChat
//
//  Created by 葱花思鸡蛋 on 2017/2/8.
//  Copyright © 2017年 葱花思鸡蛋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EaseSDKHelper.h"

@interface ChatViewController : UIViewController

/*!
 @property
 @brief 聊天的会话对象
 */
@property (strong, nonatomic) EMConversation *conversation;

@property(nonatomic,strong) NSString *username;
@end
