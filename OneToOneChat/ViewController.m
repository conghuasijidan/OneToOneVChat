//
//  ViewController.m
//  OneToOneChat
//
//  Created by 葱花思鸡蛋 on 2017/2/8.
//  Copyright © 2017年 葱花思鸡蛋. All rights reserved.
//

#import "ViewController.h"
#import <Hyphenate/Hyphenate.h>
#import "ChatViewController.h"
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *username;

@property (weak, nonatomic) IBOutlet UITextField *password;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"进入到聊天界面" forState:UIControlStateNormal];
    button.frame=CGRectMake(100, 300, 150, 50);
    [button setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];


}

-(void)buttonClick{
    
//    EaseMessageViewController *cct = [[EaseMessageViewController alloc] initWithConversationChatter:@"fdh8888" conversationType:EMConversationTypeChat];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatViewController *chatVC = [sb instantiateViewControllerWithIdentifier:@"chatStoryBoard"];
    
    [self.navigationController pushViewController:chatVC animated:YES];
    
    NSLog(@"button click");
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (IBAction)login:(id)sender {
    
    // 一般写在AppDelegate中
    [[EMClient sharedClient] loginWithUsername:self.username.text
                                      password:self.password.text
                                    completion:^(NSString *aUsername, EMError *aError) {
                                        if (!aError) {
                                            NSLog(@"登陆成功");
                                        } else {
                                            NSLog(@"登陆失败");
                                        }
                                    }];
    
    NSLog(@"请稍后正在跳转。。。");
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    ChatViewController *chatVC = segue.destinationViewController;
    chatVC.username = self.username.text;
}

- (void)dealloc
{
    NSLog(@"viewController 控制器释放");
}

@end
