//
//  DemoCallManager.h
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 22/11/2016.
//  Copyright © 2016 XieYajie. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EMCallOptions+NSCoding.h"

// 自己工程根控制器类名
#import "ViewController.h"

@class MainViewController;
@interface DemoCallManager : NSObject

#if DEMO_CALL == 1

// 自己工程根控制器 对象
@property (strong, nonatomic) ViewController *mainController;

+ (instancetype)sharedManager;

- (void)saveCallOptions;

- (void)makeCallWithUsername:(NSString *)aUsername
                        type:(EMCallType)aType;

- (void)answerCall:(NSString *)aCallId;

- (void)hangupCallWithReason:(EMCallEndReason)aReason;

#endif

@end
