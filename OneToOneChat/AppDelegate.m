//
//  AppDelegate.m
//  OneToOneChat
//
//  Created by 葱花思鸡蛋 on 2017/2/8.
//  Copyright © 2017年 葱花思鸡蛋. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "DemoCallManager.h"
#import "ChatViewController.h"
//Full版本
#import <Hyphenate/Hyphenate.h>
@interface AppDelegate ()

@end


// 注意使用数据库进行数据缓存
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];

    
    //AppKey:注册的AppKey，详细见下面注释。
    //apnsCertName:推送证书名（不需要加后缀），详细见下面注释。
    EMOptions *options =
    [EMOptions optionsWithAppkey:@"1176170208178118#onetoonechat"];
//    options.apnsCertName = @"istore_dev";
    [[EMClient sharedClient] initializeSDKWithOptions:options];

    /*先验证账户是否注册过，注册过的话就不能重复注册，直接登录，没有注册过的话，下注册，在登录*/
    
//    //环信注册
//    [[EMClient sharedClient] registerWithUsername:@"3333" password:@"123456" completion:^(NSString *aUsername, EMError *aError) {
//        if (!aError) {
//            NSLog(@"注册成功");
//        } else {
//            NSLog(@"注册失败");
//        }
//    }];
    
    
//    UIViewController *vc = [[UIViewController alloc] init];
    
//    ChatViewController *vc = [[ChatViewController alloc] init];
    
    
    // 一般写在AppDelegate中
    [[EMClient sharedClient] loginWithUsername:@"fdh3333"
                                      password:@"123456"
                                    completion:^(NSString *aUsername, EMError *aError) {
                                        if (!aError) {
                                            NSLog(@"登陆成功");
                                        } else {
                                            NSLog(@"登陆失败");
                                        }
                                    }];
    
    NSLog(@"请稍后正在跳转。。。");

    
    

    
    ViewController *vc = [[ViewController alloc] init];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    // 实时通话单例与工程根控制器关联(很重要)
    [[DemoCallManager sharedManager] setMainController:vc];
    
    self.window.rootViewController = nav;
    //
    // 实时通话单例与工程根控制器关联(很重要)
    [[DemoCallManager sharedManager] setMainController:vc];
    
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[EMClient sharedClient] applicationDidEnterBackground:application];

}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
